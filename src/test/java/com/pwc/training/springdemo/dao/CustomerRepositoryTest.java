package com.pwc.training.springdemo.dao;


import com.pwc.training.springdemo.model.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CustomerRepositoryTest {


    @Autowired
    CustomerRepository customerRepository;

    @Test
    public void when_find_all_then_return_customers() {
        customerRepository.findAll();

    }


}
