package com.pwc.training.springdemo.dao;

import com.pwc.training.springdemo.model.Customer;
import com.pwc.training.springdemo.services.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Benefits
 * a) TDD
 * b) You are not dependent on the entire Spring Container
 */
//@RuntWith(MockitoJUnitRunner.class)
@ExtendWith(MockitoExtension.class)
public class CustomerServiceTestUsingMockito {
    /**
     * The common thing that both does is
     * it creates the objects
     * InjectMock : inject the dependencies
     */
    @InjectMocks
    CustomerService service;
    @Mock
    CustomerDAO customerDAO;
    @BeforeEach
    void init() {
        Mockito
                .when(customerDAO.getCustomers())
                .thenReturn(Arrays.asList
                        (new Customer(101, "IBM", "Mumbai"),
                                new Customer(101, "Wipro", "Chennai"),
                                new Customer(101, "JPMC", "Bangalore")));
    }
    @Test
    public void when_get_customers_then_return_customer_list() {
        List<Customer> customers = service.getCustomers();
        Assert.isTrue(customers.size() > 0, "Customers must be greater than 0");
        Assert.isTrue(customers.get(0).getName().equals("IBM"), "The name expected is IBM");
    }

}
