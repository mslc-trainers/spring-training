package com.pwc.training.springdemo.dao;

import com.pwc.training.springdemo.model.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;


@SpringBootTest
public class CustomerDAOTest {

    @Autowired
    CustomerDAO customerDAO;


    @Test
    public void when_entity_manager() {
        customerDAO.getCustomersByComplexCondition();
    }

    @Test
    public void when_entity_manager_with_jpa_ql() {
        customerDAO.getCustomersByComplexConditionUsingJpaQL("PWC India");
    }



    @Test
    public void when_get_customers_then_get_customer_list() {

        List<Customer> customers = customerDAO.getCustomers();


        Assert.isTrue((customers.size() > 0), "Expected more than ZERO objects");
    }

    /**
     * Write a test case to check whether the customerDAO.getCustomer(101) works
     * as per expectations or NOT
     */
    @Test
    public void when_get_customer_then_non_null_customer() {

//        Customer customer = customerDAO.getCustomer(101);
//        Assert.isTrue(customer != null, "Customer cannot be null");
//        Assert.isTrue((customer.getName().length() > 5), "Customer name must be greater than 5 characters");

    }
}
