package com.pwc.training.springdemo.model;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class DAOMethodExecutionTimeAspect {


    public DAOMethodExecutionTimeAspect() {
        System.out.println("Aspect is instantiated...");
    }

    private static final Logger _log = LoggerFactory.getLogger(DAOMethodExecutionTimeAspect.class);

    @Around("execution(* com.pwc.training.springdemo.*..*DAO.*(..))")
//    @Around("execution(* com.pwc.training.springdemo.model.CustomerRepository.*(..))")
    public Object calculateTime(ProceedingJoinPoint joinPoint) throws Throwable {

        System.out.println("advice executed....");

        long beforeTime = System.currentTimeMillis();

        // findAll
        Object result = joinPoint.proceed();

        // some changes


        // one more change

        double afterTime = System.currentTimeMillis();
        double timeTaken = ((afterTime - beforeTime));
        //if (timeTaken >= 3000) {
        double timeTakenToDisplay = timeTaken / 1000d;
        _log.info(
                "Time taken for "
                        + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName()
                        + " execution is :" + timeTakenToDisplay + " sec(s)");
        //}
        return result;

    }
}
