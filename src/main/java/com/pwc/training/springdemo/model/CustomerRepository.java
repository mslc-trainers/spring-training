package com.pwc.training.springdemo.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {


    /**
     * Call this from DAO. Call DAO from Service. Call Service from RESTController
     *
     * Implement yet another API such that I must be able search the customers by name
     *
     *
     * @param address
     * @return
     */
    public List<Customer> findByAddress(String address);


    public List<Customer> findByNameStartingWith(String name);

    /**
     * If I type PWC, it must return to me all customers the name of which
     * starts with PWC
     *
     * @param name
     * @return
     */
//    public List<Customer> findByNameStartingWith(String name);






}
