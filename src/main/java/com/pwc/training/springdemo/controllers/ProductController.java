package com.pwc.training.springdemo.controllers;

import com.pwc.training.springdemo.dao.ProductDAO;
import com.pwc.training.springdemo.model.Product;
import com.pwc.training.springdemo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

//@Controller
public class ProductController {

    @Autowired
    ProductService productService;


    @RequestMapping (path ="/products")

    public void handleGetProducts(HttpServletRequest request, HttpServletResponse response) {

        List<Product> products = productService.getProducts();

        products.forEach(System.out::println);

    }
}
