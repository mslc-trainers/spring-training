package com.pwc.training.springdemo.services;

public class CustomerNotFoundException extends Exception {

    public CustomerNotFoundException() {

    }

    public CustomerNotFoundException(String msg) {
        super(msg);

    }

}
