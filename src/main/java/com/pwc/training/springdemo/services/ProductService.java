package com.pwc.training.springdemo.services;

import com.pwc.training.springdemo.dao.ProductDAO;
import com.pwc.training.springdemo.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    ProductDAO productDAO;

    public List<Product> getProducts() {
        return productDAO.getProducts();
    }

}
