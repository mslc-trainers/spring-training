package com.pwc.training.springdemo.services;

import com.pwc.training.springdemo.dao.CustomerDAO;
import com.pwc.training.springdemo.model.Customer;
import com.pwc.training.springdemo.model.CustomerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Business Logic to be implemented here for all business methods
 * with respect Customer
 */
@Service
public class CustomerService {


    @Autowired
    CustomerDAO customerDAO;


    public List<Customer> getCustomers() {

        System.out.println("------ getCustomers() method in CustomerService is executing");

        /**
         *
         * May be 10 more lines of code
         *
         */
        return customerDAO.getCustomers();

    }

    public List<Customer> getCustomersByAddress(String address) {
        return customerDAO.getCustomersByAddress(address);
    }

    public List<Customer> getCustomersByName(String name) {
        return customerDAO.getCustomersByName(name);
    }

    public Customer getCustomer(int id) throws CustomerNotFoundException {

        Optional<Customer> customerOptional = customerDAO.getCustomer(id);

        if (customerOptional.isPresent()) {
            return customerOptional.get();
        } else {
            throw new CustomerNotFoundException("Customer with ID : " + id + " not found");
        }


    }


    public Customer updateCustomerName(int id, String newName) {
        return customerDAO.updateName(id, newName);
    }


    public Customer createCustomer(CustomerVO customerVO) {

        /**
         *
         * Here I will check the validity of the name, the address and ofcourse Id as well.
         */

        Customer customer = customerDAO.createCustomer(customerVO.getId(),
                customerVO.getName(),
                customerVO.getAddress());

        return customer;


    }

}
