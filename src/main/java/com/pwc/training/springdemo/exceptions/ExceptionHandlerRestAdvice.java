package com.pwc.training.springdemo.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlerRestAdvice {

    @ExceptionHandler(APIException.class)
    public ResponseEntity<Map<String, String>> handleAPIException(APIException ex, HttpServletRequest request) {

        System.out.println("handleAPIException is executed.........");

//        APIException responseException =
//                new APIException(ex.getMessage(),
//                        request.getRequestURI(),
//                        "My Custom Description");

        Map<String, String> response = new HashMap<>();
        response.put("error", ex.getMessage());
        response.put("path", request.getRequestURI());
        response.put("errorDescription", "My Custom Message");

        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }


}
