package com.pwc.training.springdemo.exceptions;

public class APIException extends RuntimeException {

    private String path;
    private String errorDescription;

    public APIException() {

    }

    public APIException(String message) {
        this(message, "<< Not Defined >>", "<< Not Defined >>");

    }

    public APIException(String message, String path, String errorDescription) {
        super(message);
        this.path = path;
        this.errorDescription = errorDescription;

    }


    public String getPath() {
        return path;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
