package com.pwc.training.springdemo.restcontrollers;


import com.pwc.training.springdemo.exceptions.APIException;
import com.pwc.training.springdemo.model.Customer;
import com.pwc.training.springdemo.model.CustomerVO;
import com.pwc.training.springdemo.services.CustomerNotFoundException;
import com.pwc.training.springdemo.services.CustomerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
public class CustomerRestController {


    @Autowired
    CustomerService customerService;


    /**
     * http://localhost:8081/swagger-ui.html
     *
     * @return
     */
    @RequestMapping(path = "/customers", method = {RequestMethod.GET})
    @ApiOperation(value = "/customers", notes = "This is the API that is used to return the list of Customers")
    public List<Customer> handleGetCustomers() {

        return customerService.getCustomers();

    }

    @RequestMapping(path = "/customers/{id}", method = {RequestMethod.GET})
    public ResponseEntity<Customer> handleGetCustomer(@PathVariable Integer id)  {

        try {
            Customer customer = customerService.getCustomer(id);

            return new ResponseEntity<Customer>(customer, HttpStatus.OK);
        } catch (CustomerNotFoundException e) {
            throw new APIException(e.getMessage());
//            return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);


        }


//        return customerService.getCustomer(id);

    }

    /**
     * http://localhost:8081/swagger-ui.html
     * <p>
     * Implement all the other APIs without any body in the method
     * Just return null from the method. But the method signature must be perfect
     * <p>
     * GET - http://localhost:8080/customers?address=Mumbai (all customers where address = Mumbai)
     * <p>
     * POST - http://localhost:8080/customers (create new customer)
     * Use @RequestBody
     * <p>
     * PUT - http://localhost:8080/customers/101 (modify customer)
     * Use @RequestBody
     * DELETE - http://localhost:8080/customers/101 (delete customer 101)
     */


//    @RequestMapping(path = "/customers", method = {RequestMethod.GET}, params = {"address"})
    @GetMapping(path = "/customers-by-address", params = {"address"})
    public ResponseEntity<List<Customer>> handleGetCustomersByAddress(@RequestParam String address) {

        List<Customer> customers = customerService.getCustomersByAddress(address);

        return new ResponseEntity<>(customers, HttpStatus.OK);

    }

    @GetMapping(path = "/customers-by-name", params = {"name"})
    public ResponseEntity<List<Customer>> handleGetCustomersByName(@RequestParam String name) {

        List<Customer> customers = customerService.getCustomersByName(name);

        return new ResponseEntity<>(customers, HttpStatus.OK);

    }

//    @RequestMapping (path ="/customers", method = RequestMethod.POST)

    @PostMapping(path = "/customers", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Customer handlePostCustomer(@RequestBody CustomerVO customer) {

        System.out.println(customer.getId() + " -- " + customer.getName() + " -- " + customer.getAddress());
        // Call a service >> DAO >> Repository  [.save(entity)]


        return null;

    }

    @PutMapping(path = "/customers")
    public Customer handlePutCustomer(@RequestBody Customer customer) {


        return null;

    }

    @DeleteMapping(path = "/customers/{id}")
    public void handlePutCustomer(@PathVariable Integer id) {


        return;

    }


}
