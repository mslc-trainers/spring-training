package com.pwc.training.springdemo.dao;

import com.pwc.training.springdemo.model.Product;
import com.pwc.training.springdemo.model.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductDAO {

    @Autowired
    ProductRepository productRepository;

    public List<Product> getProducts() {

        return productRepository.findAll();
    }
}
